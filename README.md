# Introduction

This is a submod for Victoria 3 Anbennar. It contains a prototype of a magic system, including GUI.

### TODO: 
* x button on magic panel doesn't close panel
* add variables for each country for their magic school

# Notes on changes

I've kept tabs on the files changed or added, and why, so that it's hopefully easy for others to pick up and understand.

### GUI images
 * **altered** `gfx\interface\main_hud\sidebar_bg.dds` This file handles the background GUI for the buttons on the left hand of the screen. I extended the image by copy and pasting one of the button slots, to allow for a new magic button. This means that the GUI is now 50px longer.
 * **new** `gfx\interface\main_hud\magic_btn.dds`This is a temporary icon I added of a purple flame, which is the icon for the new button.

### GUI defines
 * **altered** `gui\information_panel_bar.gui`This is the main file for controlling the buttons on the left of the screen. 
At the top of the file I added a new position marker for the magic button, and also added 50 to the positions of all of the buttons below the magic button to make them fit the extended sidebar_bg.dds
The sidebar_bg.dds is loaded on line 86, so I made a small change there, change the size of the image from `size = { 50 620 }` to `size = { 50 670 }`
The actual logic for the magic button starts around line 327, at the comment `### Magic`. This is mostly copied logic from the other buttons on the file, so that it behaves the same. We reference here a variable called `magic_panel_visible` which controls whether the magic panel is currently displayed or not.
 Normal behaviour for the buttons is that if you have one already open, clicking a different button closes your open window and opens the one you just clicked. To enable this for our new magic panel I added a line to every other button `onclick = "[GetVariableSystem.Clear('magic_panel_visible')]" #Anbennar` which removes the magic panel.
  * **altered** `gui\block_windows.gui`Just a small alteration here at the bottom of the file. The magic panel is fullscreen, so we want to hide various UI elements like the outliner when it is open. I added an extra OR to the logic checking for the existence of the `magic_panel_visible` variable.
  * **altered** `gui\ingame_hud.gui` small addition on line 49 enabling the magic panel
  * **new** `gui\magic_panel.gui` TODO
  * **new** `gui\magic_panel_default.gui` TODO

### Scripting

* 